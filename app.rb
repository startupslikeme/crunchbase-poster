require 'csv'
require 'httparty'

API_KEY = ENV.fetch("INTERNAL_API_KEY")
BASE_URL = ENV.fetch("BASE_URL", "http://api.startupslike.me")

CSV.foreach("./organizations.csv", headers: true) do |row|
  payload = {
    name: row["name"],
    website_url: row["homepage_url"],
    image_url: row["profile_image_url"],
    location_city: row["location_city"],
    location_region: row["location_region"],
    location_country_code: row["location_country_code"],
    description: row["short_description"],
    api_key: API_KEY
  }.to_json
  puts HTTParty.post("#{BASE_URL}/v1/startups", body: payload, headers: {'Content-Type' => 'application/json'}).body
end
