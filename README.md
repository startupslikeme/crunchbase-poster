crunchbase-poster
=================

crunchbase-poster is a tool that reads the Crunchbase ODM startup list csv file and posts it to the API backend.

## Download the dataset

You can get the user key by registering as a basic plan user on the website.

```bash
$ wget "https://api.crunchbase.com/v/3/odm/odm.csv.tar.gz?user_key=user_key"
```

Download the file, extract it, and you'll get `organizations.csv`, which is what we need.


## Install

```bash
$ bundle install
```

## Run

`INTERNAL_API_KEY` is set in the core API server's environment variable.

```bash
$ BASE_URL=http://startupslike.me INTERNAL_API_KEY=(key) bundle exec ruby app.rb
```
